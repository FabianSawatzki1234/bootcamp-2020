package lesson2;

public class Lesson_02_05 {

  public static void main(String[] args) {
    // Implementiere einen Preisrechner, der _einmalig_ einen Rabatt von 2 Euro gibt, wenn
    // 1. Die Person 15 Jahre oder jünger ist
    // 2. Die Person über 60 Jahre ist
    // 3. Die Person ein Schüler ist
    // Gib am Ende den Ticketpreis auf der Konsole aus
    int ticketPrice = 10;
    int age = 10;
    boolean isStudent = true;

    if (age <= 15 || age > 60 || isStudent) {
      ticketPrice -=2;
    }

    System.out.println(ticketPrice);
  }
}
