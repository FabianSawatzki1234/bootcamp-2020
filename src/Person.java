public class Person {
  int age;
  String name;
  int height;

  Person(String givenName, int givenHeight) {
    this(23, givenName, givenHeight);
  }

  Person(int givenAge, String givenName, int givenHeight) {
    this.age = givenAge;
    this.name = givenName;
    this.height = givenHeight;
  }

  public void hasBirthday() {
    age += 1;
  }

  @Override
  public String toString() {
    return "Person{" +
            "age=" + age +
            ", name='" + name + '\'' +
            ", height=" + height +
            '}';
  }

  public static void main(String[] args) {
    Person anna = new Person(23, "Anna", 167);
    anna.hasBirthday();
    System.out.println(anna.name);
    System.out.println(anna.age);

    Person viktor = new Person(27, "Viktor", 182);
    System.out.println(viktor);
  }
}
