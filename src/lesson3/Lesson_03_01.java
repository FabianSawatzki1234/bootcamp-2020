package lesson3;

public class Lesson_03_01 {

  public static void main(String[] args) {
    // Schreibe den Refrain in eine Funktion und rufe diese auf, sodass kein duplizierter Code mehr vorhanden ist.

    System.out.println("It's the eye of the tiger, it's the dream of the fight");
    System.out.println("Risin' up to the challenge of our rival");
    System.out.println("And the last known survivor stalks his prey in the night");
    System.out.println("And he's watchin' us all with the eye of the tiger");

    System.out.println("Risin' up, straight to the top");
    System.out.println("Had the guts, got the glory");
    System.out.println("Went the distance, now I'm not gonna stop");
    System.out.println("Just a man and his will to survive");

    System.out.println("It's the eye of the tiger, it's the dream of the fight");
    System.out.println("Risin' up to the challenge of our rival");
    System.out.println("And the last known survivor stalks his prey in the night");
    System.out.println("And he's watchin' us all with the eye of the tiger");
  }
}
