package lesson1;

public class Lesson_01_05 {

    /* Noch mehr Datentypen!

    für ganze Zahlen: byte, short, int, long
    für Gleitkommazahlen: float, double
    für Zeichen: char
    für Wahrheitswerte: boolean

    String ist ein Sonderfall und gehört eigentlich nicht zu den sogenannten "primitiven Datentypen"
  */
    public static void main(String[] args) {
        // Aufgabe: kommentiere die Zeilen ein und versuche den richtigen Datentyp zu finden

        // boolean active = 100;
        // int genre = "minimal";
        // String sign = '+';
        // long count = 1.5;
        // char name = 'myName';

        // Aufgabe: schau dir die folgende Ausgabe an. Wieso ist das Ergebnis 2? Wie kekommen wir 2.4 heraus?
        System.out.println("ungefähr: " + 12 / 5);
        System.out.println("genau: " + 12 / 5);
    }
}
