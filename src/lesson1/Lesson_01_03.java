package lesson1;

public class Lesson_01_03 {

  public static void main(String[] args) {
  /* Ein Freund von dir scheint ziemlich gut in Musik zu sein, er hat 96% in seiner letzten Prüfung erreicht!
     Jetzt wo du alle 5 Abschlussnoten kennst, ergänze die untere Durchschnittsberechnung.
  */
    double maths = 97.5;
    double english = 98;
    double science = 83.5;
    double drama = 75;
    double sum = maths + english + science + drama;
    double average = sum / 4;
    System.out.println(average);
  }
}
