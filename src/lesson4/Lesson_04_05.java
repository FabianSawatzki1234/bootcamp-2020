package lesson4;

import static common.Helper.playRoulette;

public class Lesson_04_05 {

  public static void main(String[] args) {
    // Implementiere das Martingale-Spiel: Du besitzt Geldsumme von 40000 Dollar und das Ziel besteht darin, den Betrag auf
    // 80000 Dollar zu verdoppeln (40000 Dollar Gewinn). Der Minimaleinsatz beträgt 1 Dollar. Wann immer du verlierst,
    // verdoppelst du deinen Einsatz, um den bisherigen Verlust auszugleichen und trotzdem einen Gewinn in Höhe des
    // Minimaleinsatzes zu machen.
    // Solltest du gewonnen haben, startest du erneut mit dem Minimaleinsatz von 1 Dollar. Die Funktion playRoulette() sagt dir
    // jedes Mal, ob du gewonnen hast. Der Maximaleinsatz am Tisch beträgt 2048 Dollar: Das sollte jedoch kein Problem sein - oder?
  }

  public void martingale() {
    int money = 40000;
    int target = 80000;
    int bet = 1;
    boolean win = playRoulette();
  }
}
