package lesson4;

public class Lesson_04_04 {

  public static void main(String[] args) {
    // Nutze deine "rollDice" Funktion aus Lesson_03_04 um eine neue Funktion "keepRolling" zu implementieren. Diese neue
    // Funktion soll 5 Würfel (6 Seiten) würfeln, bis ein Kniffel geschafft wurde. Es müssen immer alle 5 Würfel gleichzeitig
    // gewürfelt werden. Außerdem soll die Funktion die Anzahl der Versuche zählen, die gebraucht wurden, um einen Kniffel zu
    // erreichen. Was ist die durchschnittliche Anzahl an Versuchen, die benötigt werden?
  }
}
